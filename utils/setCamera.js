const PiCamera = require('pi-camera');
const moment = require('moment');

const takePhoto = async (options = {}) => {
  return new Promise((resolve, reject) => {
    console.log({ options });
    const myCamera = new PiCamera({
      mode: 'photo',
      output: `${process.cwd()}/${moment()}.jpg`,
      width: 640,
      height: 480,
      // mode: 'video',
      // width: 1920,
      // height: 1080,
      // timeout: 5000, // Record for 5 seconds
      nopreview: true,
      ...options,
    });
    myCamera
      .snap()
      .then(result => resolve(result))
      .catch(error => reject(error));
  });
};

const recordVideo = async (options = {}) => {
  return new Promise((resolve, reject) => {
    const myCamera = new PiCamera({
      mode: 'video',
      output: `${__dirname}/${moment()}.h264`,
      width: 1920,
      height: 1080,
      timeout: 5000, // Record for 5 seconds
      nopreview: true,
      ...options,
    });

    myCamera
      .record()
      .then(result => resolve(result))
      .catch(error => reject(error));
  });
};

module.exports = { takePhoto, recordVideo };
