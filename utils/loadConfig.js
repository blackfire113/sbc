const path = require('path');
const awaitFs = require('await-fs');
const { machineId } = require('node-machine-id');
const { setSignalGpio } = require('./signalGpio');

module.exports = function loadConfig(location) {
  return new Promise(async (resolve, reject) => {
    const id = await machineId();
    const fileName = `${id}.conf`;

    try {
      let config = await awaitFs.readFile(fileName, 'utf8');
      config = JSON.parse(config);
      if (config && config.gpio) {
        const { gpio } = config;
        gpio.forEach(({ port, value }) => {
          return setSignalGpio({
            port,
            value,
          });
        });
      }
    } catch (exception) {
      console.log('Problemas para escribir el archivo de status');
      console.log({ exception });
      // reject(exception);
    }
  });
};
