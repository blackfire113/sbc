const { Gpio } = require('pigpio');

/**
 *
 * @param {Number Gpio} port
 * @param {Number} // dutyCycle
 */
const setServo = ({ port, value }) => {
  // 5 to 254 dutyCycle
  if (process.env.TEST) {
    console.log('Sistema de prueba');
    return;
  }
  const servo = new Gpio(port, {
    mode: Gpio.OUTPUT,
  });
  console.log('test 2');
  servo.pwmWrite(value);
  console.log('test 3');
  return servo.unexport();
};

module.exports = { setServo };
