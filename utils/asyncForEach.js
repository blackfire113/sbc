/* eslint-disable no-await-in-loop */
// async/await foreach
module.exports = async function asyncForEach(array, cb) {
  if (!Array.isArray(array)) {
    throw new Error('asyncForEach: The first argument must be an Array');
  }
  if (typeof cb !== 'function') {
    throw new Error('asyncForEach: The second argument must be a function');
  }
  for (let index = 0; index < array.length; index++) {
    await cb(array[index], index, array);
  }
};
