const axios = require('axios');
const { machineId } = require('node-machine-id');

module.exports = async function sendSbcRequest() {
  const { API_HOST, EMAIL } = process.env;
  const id = await machineId();
  let hostApi = `${API_HOST}/sbc-register`;
  try {
    const response = await axios.post(hostApi, { machineId: id, email: EMAIL });
    const { data } = response;
    token = data.token;
    status = response.status;
  } catch (exception) {}
};
