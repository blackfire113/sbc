const { Gpio } = require('onoff');

const setSignalGpio = (options = {}) => {
  const { port, time, value } = options;
  // port number
  // status 1 or 0
  let gpio;
  if (Gpio.accessible) {
    gpio = new Gpio(port, 'out');
    // more real code here
  } else {
    gpio = {
      writeSync: (value) => {
        console.log('VIRTUAL VALUE:', value);
      },
    };
  }
  if (value) {
    gpio.writeSync(value);
  } else {
    if (Gpio.accessible) {
      gpio.unexport();
    }
    console.log('VIRTUAL VALUE:', value);
  }
  return;
  // try {
  //   const gpio = new Gpio(port, 'out');
  //   if (gpio.readSync() !== value) {
  //     gpio.writeSync(value);
  //   }
  //   return gpio.unexport();
  // } catch (e) {
  //   const { errno } = e;

  //   if (errno === -13 || errno === -30) {
  //     console.log('No es un dispositívo raspberry');
  //   } else {
  //     console.error({ e });
  //   }
  // }
};

const statusGpio = (port) => {
  //port number
  try {
    const gpio = new Gpio(port, 'in');
    const status = gpio.readSync();
    gpio.unexport();
    return status;
  } catch (e) {
    const { errno } = e;

    if (errno === -13 || errno === -30) {
      console.log('No es un dispositívo raspberry');
    } else {
      console.error({ e });
    }
  }
};

module.exports = { statusGpio, setSignalGpio };
