const chalk = require('chalk');
const { compose } = require('compose-middleware');

module.exports = async function eventToSockets(events) {
  console.log({ events });
  Object.keys(events).forEach(eventName => {
    const event = events[eventName];
    if (!Array.isArray(event.middlewares)) event.middlewares = [];

    // Se obtienen las acciones de los eventos, filtrando el array de middlewares
    const actions = Object.keys(event).filter(item => item !== 'middlewares');

    actions.forEach(actionName => {
      const action = event[actionName];
      let eventAction = {
        middlewares: event.middlewares,
        globalMiddlewares: true,
        eventMiddlewares: true,
      };

      if (typeof action === 'function') {
        eventAction.handler = action;
      } else {
        eventAction = { ...eventAction, ...action };
      }
      const ctrlMiddlewares = eventAction.eventMiddlewares
        ? [...event.middlewares, ...eventAction.middlewares]
        : [];

      const { globalMiddlewares } = eventAction;

      const middlewares = globalMiddlewares
        ? [
            ...(Array.isArray(global.middlewares) ? global.middlewares : []),
            ...(Array.isArray(ctrlMiddlewares) ? ctrlMiddlewares : []),
          ]
        : ctrlMiddlewares;
    });
  });
  console.log(chalk.bold.blue('>> Socket events loaded successfully'));
  return router;
};
