const axios = require('axios');
const { machineId } = require('node-machine-id');
const sleep = require('./sleep');

const getToken = async () => {
  const { LOGIN_HOST } = process.env;
  const serverTestLogin = 'http://login:3050';
  const serverSocketTest = 'http://socket:3003/socket.io';
  const serverApiTest = 'http://api:3000';
  const id = await machineId();
  console.log('id:', id);
  let token;
  let status;
  do {
    try {
      let hostLogin = `${LOGIN_HOST}/auth/token?machineId=${id}`;
      const response = await axios.get(hostLogin);
      const { data } = response;
      token = data.token;
      status = response.status;
    } catch (exception) {}
    if (!token) {
      try {
        hostLogin = `${serverTestLogin}/auth/token?machineId=${id}`;
        const response = await axios.get(hostLogin);
        const dataTest = response.data;
        token = dataTest.token;
        status = response.status;
        process.env.SOCKET_HOST = serverSocketTest;
        process.env.API_HOST = serverApiTest;
      } catch (exception) {}
    }
    if (status !== 200) {
      await sleep(8000);
    }
  } while (!token);
  return token;
};

module.exports = getToken;
