const axios = require('axios');
const { machineId } = require('node-machine-id');
const sleep = require('./sleep');

const getToken = async () => {
  const { LOGIN_HOST, LOGIN_PORT } = process.env;
  const serverTestLogin = 'login';
  const serverSocketTest = 'socket';
  const serverApiTest = 'api';
  const id = await machineId();
  console.log('id:', id);
  let token;
  let status;
  do {
    try {
      let hostLogin = `http://${LOGIN_HOST}:${LOGIN_PORT}/auth/token?machineId=${id}`;
      const response = await axios.get(hostLogin);
      const { data } = response;
      token = data.token;
      status = response.status;
    } catch (exception) {}
    if (!token) {
      try {
        hostLogin = `http://${serverTestLogin}:${LOGIN_PORT}/auth/token?machineId=${id}`;
        const response = await axios.get(hostLogin);
        const dataTest = response.data;
        token = dataTest.token;
        status = response.status;
        process.env.SOCKET_HOST = serverSocketTest;
        process.env.HOST = serverTestLogin;
        process.env.API_HOST = serverApiTest;
      } catch (exception) {}
    }
    if (status !== 200) {
      await sleep(8000);
    }
  } while (!token);
  return token;
};

module.exports = getToken;
