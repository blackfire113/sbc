const { Gpio } = require('onoff');
const gpios = [
  2,
  3,
  4,
  17,
  27,
  22,
  10,
  9,
  11,
  5,
  6,
  13,
  19,
  26,
  14,
  15,
  18,
  23,
  24,
  25,
  8,
  7,
  12,
  16,
  20,
  21,
];
module.exports = () => {
  console.log('>>>>>> exit <<<<<');
  if (Gpio.accessible) {
    gpios.forEach(port => {
      new Gpio(port).unexport();
    });
  }
};
