module.exports = (app, err) => {
  console.log({ err });
  console.error(`>> Fatal error: ${err.message}`);
  console.error(err.stack);
  if (app.sequelize) app.sequelize.close();
  process.exit(1);
};
