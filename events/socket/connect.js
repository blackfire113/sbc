const fs = require('fs');
const awaitFs = require('await-fs');
const { machineId } = require('node-machine-id');

module.exports = async () => {
  console.log('>>>>>> connected <<<<<');
  const id = await machineId();
  const fileName = `${id}.conf`;
  if (!fs.existsSync(fileName)) {
    //Consultar al servidor mi status actual
    return;
  }
  try {
    const status = await awaitFs.readFile(fileName, 'utf8');
    console.log({ status });
    // TODO: Realizar acción por los status
  } catch (err) {
    console.log({ err });
  }
};
