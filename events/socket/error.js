module.exports = error => {
  console.log('>>>>> error <<<<<');
  console.log({ error });
  if (error.type === 'UnauthorizedError' || error.code === 'invalid_token') {
    // redirect user to login page perhaps?
    console.log("User's token has expired");
  }
};
