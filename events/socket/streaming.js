// const cv = require('opencv4nodejs');
// const NodeWebcam = require('node-webcam');
const { StreamCamera, Codec } = require('pi-camera-connect');
const fs = require('fs');
const sleep = require('../../utils/sleep');
const { setSignalGpio } = require('../../utils/signalGpio');
const { setServo } = require('../../utils/servo');
const { takePhoto, recordVideo } = require('../../utils/setCamera');
const asyncForEach = require('../../utils/asyncForEach');
const loadEvents = require('../../utils/loadEvents');

module.exports = function (data) {
  console.log('>>>>>> streaming <<<<<<<');
  const { status, port = 0, userId = [], actionId } = data;
  const { STREAMING } = process.env;
  // status =  'start' , 'stop'
  setTimeout(async () => {
    const eventsProcess = await loadEvents('process');
    Object.keys(eventsProcess).forEach((eventName) => {
      const [event] = eventName.split('.');
      process.on(event, eventsProcess[eventName]);
    });

    const streamCamera = new StreamCamera({
      codec: Codec.MJPEG,
    });

    if (status === 'stop') {
      process.env.STREAMING = 'false';
    }

    if (status === 'start') {
      process.env.STREAMING = 'true';

      await streamCamera.startCapture();
      const timerId = setInterval(async () => {
        try {
          if (process.env.STREAMING === 'false') {
            await streamCamera.stopCapture();
            this.emit('streaming', { actionId, userId, image: '' });
            clearInterval(timerId);
          }

          const image = await streamCamera.takeImage();
          this.emit('streaming', { actionId, userId, image: image.toString('base64') });
        } catch (exception) {
          console.log({ exception });
        }
      }, 500);
    }
  }, 1);
};

// Old version
// const wCap = new cv.VideoCapture(port);
// const webcam = NodeWebcam.create(opts);

// mala version
// webcam.capture('webcam', (err, image) => {
//   this.emit('streaming', { userId, image });
// });

// old version
//const image = cv.imencode('.jpg', wCap.read()).toString('base64');
// this.emit('streaming', { userId, image });
