module.exports = (error, callback) => {
  console.log('>>>>> unauthorized <<<<<');
  console.log({ error });
  console.log({ callback });
  if (error.data.type === 'UnauthorizedError' || error.data.code === 'invalid_token') {
    // redirect user to login page perhaps or execute callback:
    callback();
    console.log("User's token has expired");
  }
};
