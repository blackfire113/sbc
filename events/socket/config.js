const awaitFs = require('await-fs');
const { machineId } = require('node-machine-id');

module.exports = async data => {
  console.log('>>>>> config <<<<<');
  const id = await machineId();
  const fileName = `${id}.conf`;
  console.log({ data });
  try {
    await awaitFs.writeFile(fileName, JSON.stringify(data));
  } catch (exception) {
    console.log('Problemas para escribir el archivo de status');
    console.log({ exception });
  }

  if (data) {
    const { gpio } = data;

    data.forEach(({ port, value }) => {
      setSignalGpio({
        port,
        value,
      });
    });
  }
};
