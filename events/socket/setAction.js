const sleep = require('../../utils/sleep');
const { setSignalGpio } = require('../../utils/signalGpio');
const { setServo } = require('../../utils/servo');
const { takePhoto, recordVideo } = require('../../utils/setCamera');
const asyncForEach = require('../../utils/asyncForEach');
const loadEvents = require('../../utils/loadEvents');

module.exports = function (data) {
  console.log('>>>>>> setAction <<<<<<<');
  const { states, frequency } = data;
  console.log({ data });
  console.log({ states });
  let indexFrequency = frequency || 1;
  setTimeout(async () => {
    const eventsProcess = await loadEvents('process');
    Object.keys(eventsProcess).forEach((eventName) => {
      const [event] = eventName.split('.');
      process.on(event, eventsProcess[eventName]);
    });
    do {
      await asyncForEach(states, async (state) => {
        const { value, mode } = state;
        if (value > 1) {
          // Señales pwm value entre 5 y 254
          setServo({
            port: state.gpio,
            value,
          });
        } else {
          // value entre 1 y 0
          setSignalGpio({
            port: state.gpio,
            value,
          });
        }

        const { time } = state;
        await sleep(time || 0);
      });
      indexFrequency--;
      console.log({ indexFrequency });
    } while (frequency === 0 || indexFrequency > 0);
    this.emit('updateAction', { _id: data._id, status: 'inactive' });
  }, 1);
};
