const io = require('socket.io-client');
const dotenv = require('dotenv');
const path = require('path');
const ping = require('ping');
const loadEvents = require('./utils/loadEvents');
const getToken = require('./utils/getToken');
const sleep = require('./utils/sleep');
const loadConfig = require('./utils/loadConfig');
const sendSbcRequest = require('./utils/sendSbcRequest');

dotenv.config({
  path: path.resolve(process.cwd(), '.env'),
});

const init = async () => {
  // getToken loop
  // await loadConfig();
  const token = await getToken();
  const eventsProcess = await loadEvents('process');
  Object.keys(eventsProcess).forEach((eventName) => {
    const [event] = eventName.split('.');
    process.on(event, eventsProcess[eventName]);
  });
  if (process.env.EMAIL) {
    await sendSbcRequest();
  }
  const eventsSocket = await loadEvents('socket');
  const { SOCKET_HOST, SOCKET_PORT } = process.env;
  console.log({ SOCKET_HOST, SOCKET_PORT });
  const hostSocket = `http://${SOCKET_HOST}:${SOCKET_PORT}`;
  const socket = io.connect(hostSocket, {
    query: `token=${token}`,
    reconnection: true,
  });
  Object.keys(eventsSocket).forEach((eventName) => {
    const [socketEvent] = eventName.split('.');
    socket.on(socketEvent, eventsSocket[eventName]);
  });
};
init();
