const io = require('socket.io-client');
const dotenv = require('dotenv');
const path = require('path');
const ping = require('ping');
const loadEvents = require('./utils/loadEvents');
const getToken = require('./utils/getToken');
const sleep = require('./utils/sleep');
const loadConfig = require('./utils/loadConfig');
const sendSbcRequest = require('./utils/sendSbcRequest');

dotenv.config({
  path: path.resolve(process.cwd(), '.env'),
});

const init = async () => {
  // getToken loop
  const token = await getToken();
  const eventsProcess = await loadEvents('process');
  Object.keys(eventsProcess).forEach((eventName) => {
    const [event] = eventName.split('.');
    process.on(event, eventsProcess[eventName]);
  });
  console.log({ email: process.env.EMAIL });
  if (process.env.EMAIL) {
    await sendSbcRequest();
  }
  const eventsSocket = await loadEvents('socket');
  const { SOCKET_HOST } = process.env;

  const socket = await io.connect(SOCKET_HOST, {
    query: `token=${token}`,
    reconnection: true,
    secure: true,
  });

  Object.keys(eventsSocket).forEach((eventName) => {
    const [socketEvent] = eventName.split('.');
    socket.on(socketEvent, eventsSocket[eventName]);
  });
};
init();
